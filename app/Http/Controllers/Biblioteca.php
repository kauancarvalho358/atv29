<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Livros;

use App\Models\Editoras;

use App\Models\Autores;

class Biblioteca extends Controller
{
    public function retorno(){
        $livro = Livros::all();
        $editora = Editoras::all();
        $autor = Autores::all();
        return view('listagemLivros',['livro'=>$livro, 'editora'=>$editora, 'autor'=>$autor]);

    }
}
