<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Biblioteca/Paulo</title>
    </head>
    <body>
        <h1>Biblioteca</h1>
        <h3>Tabela de Livros</h3>
        <table  border='1'>
            <tr>
                <td>
                    <h4> ID do livro:</h4> 
                </td>
                <td>
                    <h4>Título do livro:</h4>
                </td>
                <td> 
                    <h4>ID do autor:</h4>
                </td>
                <td> 
                    <h4>ID da editora:</h4>
                </td>
            </tr>
            @foreach($livro as $livro)
            <tr>
                <td>
                    {{$livro->id}}
                </td>
                <td>
                    {{$livro->livro}}
                </td>
                <td>
                    {{$livro->autor}}
                </td>
                <td>
                    {{$livro->editora}}
                </td>
            </tr>
            @endforeach
        </table>
        <br>
        <h3>Tabela de Editoras</h3>
        <table border='1'>
            <tr>
                <td>
                    <h4>ID da editora:</h4>
                </td>
                <td>
                    <h4>Nome da editora:</h4>
                </td>
            </tr>
            @foreach($editora as $editora)
            <tr>
                <td>
                    {{$editora->id}}
                </td>
                <td>
                    {{$editora->editora}}
                </td>
            </tr>
            @endforeach
        </table>
        <br>
        <h3>Tabela Autores</h3>
        <table border='1'>
            <tr>
                <td>
                    <h4>ID do Autor:</h4>
                </td>
                <td>
                    <h4>Nome do Autor:</h4>
                </td>
            </tr>
            @foreach($autor as $autor)
            <tr>
                <td>
                    {{$autor->id}}
                </td>
                <td>
                    {{$autor->autor}}
                </td>
            </tr>
            @endforeach
        </table>  
    </body>
</html>
